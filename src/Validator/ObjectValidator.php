<?php

namespace App\Validator;

use App\Exception\InvalidArgumentException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ObjectValidator {

    /**
     *
     * @var ValidatorInterface
     */
    private $validatorInterface;

    function __construct(
            ValidatorInterface $validatorInterface
    ) {
        $this->validatorInterface = $validatorInterface;
    }

    public function validateEntity($entity) {

        $errors = $this->validatorInterface->validate($entity);
        return $this->throwErrorException($errors);
    }

    private function throwErrorException($errors) {

        $errorsResponse = '';
        if (count($errors) > 0) {

            foreach ($errors as $key => $error) {
                $errorsResponse .= $error->getMessage() . ". ";
            }

            throw new InvalidArgumentException($errorsResponse);
        }

        return true;
    }

}
