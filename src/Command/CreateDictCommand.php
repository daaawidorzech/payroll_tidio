<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Fixture\DictFixture;

class CreateDictCommand extends Command {

    private DictFixture $dictFixture;

    function __construct(
            DictFixture $dictFixture
    ) {
        $this->dictFixture = $dictFixture;

        parent::__construct();
    }

    protected static $defaultName = 'app:create-dict-fixture';

    protected function execute(InputInterface $input, OutputInterface $output) {

        $time_start = microtime(true);
        $output->writeln([
            'Create Dict',
            '============',
            '',
        ]);

        $output->writeln('Start!');
        $this->dictFixture->createFixtures();
        $time_end = microtime(true);
        $output->write('End.');


        $output->writeln($time_end - $time_start);


        die;
    }

}
