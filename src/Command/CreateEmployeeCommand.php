<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Fixture\EmployeeFixture;

class CreateEmployeeCommand extends Command {

    private EmployeeFixture $employeeFixture;

    function __construct(
            EmployeeFixture $employeeFixture
    ) {
        $this->employeeFixture = $employeeFixture;

        parent::__construct();
    }

    protected static $defaultName = 'app:create-employee-fixture';

    protected function execute(InputInterface $input, OutputInterface $output) {

        $time_start = microtime(true);
        $output->writeln([
            'Create Employee',
            '============',
            '',
        ]);

        $output->writeln('Start!');
        $this->employeeFixture->createFixtures();
        $time_end = microtime(true);
        $output->write('End.');


        $output->writeln($time_end - $time_start);


        die;
    }

}
