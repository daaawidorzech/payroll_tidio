<?php

namespace App\Fixture;

use App\Service\Employee\EmployeeService;
use App\Model\Dto\Employee\CreateEmployeeReq;
use App\Repository\DictDepartmentRepository;
use App\Repository\DictCurrencyRepository;

class EmployeeFixture {

    CONST TEST_EMPLOYEE_COUNT = 50;

    private EmployeeService $employeeService;
    private DictDepartmentRepository $dictDepartmentRepository;
    private DictCurrencyRepository $dictCurrencyRepository;

    function __construct(
            EmployeeService $employeeService,
            DictDepartmentRepository $dictDepartmentRepository,
            DictCurrencyRepository $dictCurrencyRepository
    ) {
        $this->employeeService = $employeeService;
        $this->dictDepartmentRepository = $dictDepartmentRepository;
        $this->dictCurrencyRepository = $dictCurrencyRepository;
    }

    public function createFixtures(): bool {

        $faker = \Faker\Factory::create();

        $departments = $this->dictDepartmentRepository->findAll();

        $departmentsIds = [];
        foreach ($departments as $department) {
            $departmentsIds[] = $department->getId();
        }

        $currencies = $this->dictCurrencyRepository->findAll();
        $currenciesIds = [];
        foreach ($currencies as $currency) {
            $currenciesIds[] = $currency->getId();
        }
      
        for ($index = 0; $index < self::TEST_EMPLOYEE_COUNT; $index++) {
            
            $randomDepartments = array_rand($departmentsIds);
            $randomCurrencies = array_rand($currenciesIds);
         
            $dataArr = [
                'firstName' => $faker->firstName,
                'lastName' => $faker->lastName,
                'departmentId' => $departmentsIds[$randomDepartments],
                'hiredAt' => $faker->dateTimeBetween('-20 years', 'now', 'Europe/Warsaw')->format("Y-m-d"),
                'basicSalary' => $faker->numberBetween(500, 11000),
                'currencyId' => $currenciesIds[$randomCurrencies],
            ];
            print_r($dataArr);
            $createEmployeeReq = new CreateEmployeeReq();
            $createEmployeeReq->fromArray($dataArr);
            $this->employeeService->createEmployee($createEmployeeReq);
        }

        return true;
    }

}
