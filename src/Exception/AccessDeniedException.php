<?php
namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException as Symfony403;

class AccessDeniedException
    extends Symfony403
{
}