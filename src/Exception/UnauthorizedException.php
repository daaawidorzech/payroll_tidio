<?php
namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException as Symfony401;

class UnauthorizedException
    extends Symfony401
{

}