<?php

namespace App\Service\Report;

use App\Model\Dto\Report\Filter;
use App\Repository\EmployeeRepository;
use App\Model\Types\ResponseTypeEnum;
use App\Service\File\FileService;
use App\Model\File\Report\ReportFile;
use App\Exception\InvalidArgumentException;

class ReportService {

    private EmployeeRepository $employeeRepository;
    private FileService $fileService;

    function __construct(
            EmployeeRepository $employeeRepository,
            FileService $fileService
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->fileService = $fileService;
    }

    public function createReport(Filter $filters) {

        $employees = $this->employeeRepository->findByFilters($filters);

        $reportFile = new ReportFile();
        $reportFile->setData($employees);

        switch ($filters->getResponseType()) {

            case ResponseTypeEnum::TYPE_RESPONSE_CSV:
                return $this->fileService->createCsv($reportFile);

            case ResponseTypeEnum::TYPE_RESPONSE_JSON:

                return array_merge($reportFile::COLUMNS, $reportFile->getData());

            case ResponseTypeEnum::TYPE_RESPONSE_XLSX:
                
                return $this->fileService->createXlsx($reportFile);

            default:
                throw new InvalidArgumentException('Invalid Response Type');
                break;
        }
    }

}
