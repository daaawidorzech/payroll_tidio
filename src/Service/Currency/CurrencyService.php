<?php

namespace App\Service\Currency;

use App\Repository\DictCurrencyRepository;
use App\Entity\DictCurrency;
use App\Exception\NotFoundException;

class CurrencyService {

    private DictCurrencyRepository $dictCurrencyRepository;

    function __construct(
            DictCurrencyRepository $dictCurrencyRepository
    ) {
        $this->dictCurrencyRepository = $dictCurrencyRepository;
    }

    public function findOne(int $currencyId): DictCurrency {

        $currency = $this->dictCurrencyRepository->find($currencyId);

        if (!$currency instanceof DictCurrency) {
            throw new NotFoundException('Currency not found');
        }

        return $currency;
    }

}
