<?php

namespace App\Service\Employee;

use App\Service\PersistenceService;
use App\Model\Dto\Employee\CreateEmployeeReq;
use App\Entity\Employee;
use App\Service\Department\DepartmentService;
use App\Service\Salary\SalaryService;
use App\Exception\InvalidArgumentException;

class EmployeeService {

    private PersistenceService $persistenceService;
    private DepartmentService $departmentService;
    private SalaryService $salaryService;

    function __construct(
            PersistenceService $persistenceService,
            DepartmentService $departmentService,
            SalaryService $salaryService
    ) {
        $this->persistenceService = $persistenceService;
        $this->departmentService = $departmentService;
        $this->salaryService = $salaryService;
    }

    public function createEmployee(CreateEmployeeReq $createEmployeeReq): bool {

        try {
            $this->persistenceService->beginTransaction();
            $employee = new Employee();
            $employee->setFirstName($createEmployeeReq->getFirstName());
            $employee->setLastName($createEmployeeReq->getLastName());
            $employee->setHiredAt($createEmployeeReq->getHiredAt());
            $employee->setDictDepartment($this->departmentService->findOne($createEmployeeReq->getDepartmentId()));

            $this->persistenceService->save($employee);

            $this->salaryService->createSalary($employee, $createEmployeeReq->getSalary());
            $this->persistenceService->commit();
        } catch (\Throwable $exc) {
            $this->persistenceService->rollback();
            throw new InvalidArgumentException("Create Employee Error");
        }

        return true;
    }

}
