<?php

namespace App\Service\Department;

use App\Repository\DictDepartmentRepository;
use App\Entity\DictDepartment;
use App\Exception\NotFoundException;
use App\Model\Dto\Department\CreateDepartmentReq;
use App\Service\PersistenceService;
use App\Service\PremiumConfig\PremiumConfigService;
use App\Exception\InvalidArgumentException;

class DepartmentService {

    private DictDepartmentRepository $departmentRepositry;
    private PersistenceService $persistenceService;
    private PremiumConfigService $premiumConfigService;

    function __construct(
            DictDepartmentRepository $departmentRepositry,
            PersistenceService $persistenceService,
            PremiumConfigService $premiumConfigService
    ) {
        $this->departmentRepositry = $departmentRepositry;
        $this->persistenceService = $persistenceService;
        $this->premiumConfigService = $premiumConfigService;
    }

    public function findOne(int $departmentId): DictDepartment {

        $department = $this->departmentRepositry->find($departmentId);

        if (!$department instanceof DictDepartment) {
            throw new NotFoundException('Department not found');
        }

        return $department;
    }

    public function createDepartment(CreateDepartmentReq $dto): bool {
        try {
            $this->persistenceService->beginTransaction();
            $department = new DictDepartment();
            $department->setName($dto->getName());
            $department->setDescription($dto->getDescription());

            $this->persistenceService->save($department);
            $this->premiumConfigService->createPremiumConfig($department, $dto);
        } catch (\Throwable $exc) {
            $this->persistenceService->rollback();
            throw new InvalidArgumentException("Create Department Error");
        }

        return true;
    }

}
