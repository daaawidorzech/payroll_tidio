<?php

namespace App\Service\PremiumConfig;

use App\Entity\PremiumConfig;
use App\Repository\PremiumConfigRepository;
use App\Entity\DictDepartment;
use App\Entity\DictCurrency;
use App\Service\Currency\CurrencyService;
use App\Model\Dto\Department\CreateDepartmentReq;
use App\Service\PersistenceService;
use App\Model\Types\PremiumTypeEnum;
use App\Exception\NotFoundException;

class PremiumConfigService {

    private PremiumConfigRepository $premiumConfigRepository;
    private CurrencyService $currencyService;
    private PersistenceService $persistenceService;

    function __construct(
            PremiumConfigRepository $premiumConfigRepository,
            CurrencyService $currencyService,
            PersistenceService $persistenceService
    ) {
        $this->premiumConfigRepository = $premiumConfigRepository;
        $this->currencyService = $currencyService;
        $this->persistenceService = $persistenceService;
    }

    public function findPremiumConfig(DictDepartment $department): PremiumConfig {

        $config = $this->premiumConfigRepository->findOneBy([
            'dictDepartment' => $department
        ]);

        if (!$config instanceof PremiumConfig) {
            throw new NotFoundException('Premium Config not found');
        }

        return $config;
    }

    public function findConstPremiumConfig(DictDepartment $department, DictCurrency $currency): PremiumConfig {

        $config = $this->premiumConfigRepository->findOneBy([
            'dictDepartment' => $department,
            'dictCurrency' => $currency
        ]);

        if (!$config instanceof PremiumConfig) {
            throw new NotFoundException('Premium Config not found');
        }

        return $config;
    }

    public function createPremiumConfig(DictDepartment $department, CreateDepartmentReq $dto): bool {
        
        $premiumConfig = new PremiumConfig();        
        $currency = ($dto->getPremiumType() == PremiumTypeEnum::TYPE_CONST_AMOUNT)? $this->currencyService->findOne($dto->getCurrencyId()):null;        
        $premiumConfig->setDictCurrency($currency);
        $premiumConfig->setDictDepartment($department);
        $premiumConfig->setType($dto->getPremiumType());
        $premiumConfig->setValue($dto->getPremiumValue());

        $this->persistenceService->save($premiumConfig);

        return true;
    }

}
