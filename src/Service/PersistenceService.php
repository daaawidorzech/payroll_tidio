<?php

namespace App\Service;

use App\Exception\InvalidArgumentException;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PersistenceService {

    /**
     * default connection
     * @var Connection
     */
    private $connection;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
            EntityManagerInterface $em,
            Connection $connection,
            ValidatorInterface $validator
    ) {
        $this->em = $em;
        $this->connection = $connection;
        $this->validator = $validator;
    }

    public function getRepository(string $entityClass) {
        return $this->em->getRepository($entityClass);
    }

    public function beginTransaction() {
        $this->em->getConnection()->beginTransaction();
    }

    public function commit() {
        $this->em->getConnection()->commit();
    }

    public function rollback() {
        if ($this->em->getConnection()->isTransactionActive()) {
            $this->em->getConnection()->rollBack();
        }
    }

    public function save($entity) {
        if ($entity->getId()) {
            $this->merge($entity);
        } else {
            $this->persist($entity);
        }

        $this->flush();
    }

    /**
     * bezpośredni update z pominięciem EM
     *
     * @param $table
     * @param $fields
     * @param int $id
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    public function directUpdate($table, $fields, int $id) {
        $columns = [];
        $values = [];
        foreach ($fields as $column => $value) {
            $columns[] = $column . '=?';
            $values[] = $value;
        }

        $setExpression = \implode(', ', $columns);
        $sql = "UPDATE $table SET $setExpression WHERE id=$id";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute($values);

        if ($stmt->errorCode() != 0) {
            throw new \Exception(print_r($stmt->errorInfo(), true));
        }
    }

    public function remove($entity) {
        $this->em->remove($entity);
    }

    public function delete($entity) {
        $this->em->remove($entity);
        $this->flush();
    }

    public function merge($entity) {
        $this->validate($entity);
        $this->em->merge($entity);
    }

    public function persist($entity) {
        $this->validate($entity);
        $this->em->persist($entity);
    }

    public function flush() {
        $this->em->flush();
    }

    private function validate($entity) {
        
        $errorsResponse='';
        $violations = $this->validator->validate($entity);
        if ($violations->count() > 0) {
            foreach ($violations as $key => $error) {
                $errorsResponse .= $error->getMessage() . ".";
            }

            throw new InvalidArgumentException($errorsResponse);
        }
    }

}
