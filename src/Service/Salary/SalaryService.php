<?php

namespace App\Service\Salary;

use App\Entity\EmployeeSalary;
use App\Repository\EmployeeSalaryRepository;
use App\Entity\Employee;
use App\Model\Dto\Salary\Salary;
use App\Service\Currency\CurrencyService;
use App\Service\PremiumConfig\PremiumConfigService;
use App\Entity\DictDepartment;
use App\Entity\DictCurrency;
use App\Model\Types\PremiumTypeEnum;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Exception\InvalidArgumentException;
use App\Service\PersistenceService;

class SalaryService {

    private EmployeeSalaryRepository $employeeSalaryRepository;
    private CurrencyService $currencyService;
    private PremiumConfigService $premiumConfigService;
    private ParameterBagInterface $params;
    private int $constAmountYears;
    private PersistenceService $persistenceService;

    function __construct(
            EmployeeSalaryRepository $employeeSalaryRepository,
            CurrencyService $currencyService,
            PremiumConfigService $premiumConfigService,
            ParameterBagInterface $params,
            PersistenceService $persistenceService
    ) {
        $this->employeeSalaryRepository = $employeeSalaryRepository;
        $this->currencyService = $currencyService;
        $this->premiumConfigService = $premiumConfigService;
        $this->params = $params;
        $this->constAmountYears = $this->params->get('salary')['constAmountYears'];
        $this->persistenceService = $persistenceService;
    }

    public function createSalary(Employee $employee, Salary $salary): bool {

        $employeeSalary = new EmployeeSalary();
        $employeeSalary->setEmployee($employee);
        $employeeSalary->setBasicSalary($salary->getBasicSalary());
        $employeeSalary->setDictCurrency($this->currencyService->findOne($salary->getCurrencyId()));

        $premium = $this->getPremium($employeeSalary->getBasicSalary(), $employee->getDictDepartment(), $employeeSalary->getDictCurrency(), $employee);
        $employeeSalary->setPremium($premium);
        $employeeSalary->setSalary($this->getSalary($employeeSalary->getBasicSalary(), $premium));

        $this->persistenceService->save($employeeSalary);

        return true;
    }

    public function getPremium(float $basicSalary, DictDepartment $department, DictCurrency $currency, Employee $employee): float {

        $config = $this->premiumConfigService->findPremiumConfig($department);

        switch ($config->getType()) {
            case PremiumTypeEnum::TYPE_CONST_AMOUNT:

                $config = $this->premiumConfigService->findConstPremiumConfig($department, $currency);
                $premium = new \App\Model\SalaryPremium\ConstAmount($config->getValue(), $employee->getHiredAt(), $this->constAmountYears);
                return $premium->getPremiumValue();

            case PremiumTypeEnum::TYPE_PERCENT_AMOUNT:

                $premium = new \App\Model\SalaryPremium\PercentAmount($basicSalary, $config->getValue());
                return $premium->getPremiumValue();

            default:
                throw new InvalidArgumentException("Invalid premium type");
        }
    }

    public function getSalary(float $basicSalary, float $premium): float {

        return floatval(bcadd($basicSalary, $premium, 2));
    }

}
