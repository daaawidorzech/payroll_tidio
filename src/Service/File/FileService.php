<?php

namespace App\Service\File;

use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\Writer\Csv;
use App\Model\File\Report\ReportFile;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FileService {

    private ParameterBagInterface $params;
    private string $tmpFolder;

    function __construct(
            ParameterBagInterface $params
    ) {
        $this->params = $params;
        $this->tmpFolder = $this->params->get('files')['tmpFilePath'];
    }

    public function createXlsx(ReportFile $reportFile) {

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()
                ->fromArray(
                        array_merge($reportFile::COLUMNS, $reportFile->getData()),
                        NULL,
                        'A1',
                        true
        );
        $fileName = $reportFile->getFileName() . date("_Ymd_His") . '.xlsx';
        $fullPath = $this->params->get('kernel.project_dir') . $this->tmpFolder . $fileName;

        $writer = new Xlsx($spreadsheet);
        $writer->save($fullPath);

        $reportFile->setFullPath($fullPath);
        $reportFile->setFileName($fileName);

        return $reportFile;
    }

    public function createCsv(ReportFile $reportFile) {

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()
                ->fromArray(
                        array_merge($reportFile::COLUMNS, $reportFile->getData()),
                        NULL,
                        'A1',
                        true
        );
        $fileName = $reportFile->getFileName() . date("_Ymd_His") . '.csv';
        $fullPath = $this->params->get('kernel.project_dir') . $this->tmpFolder . $fileName;

        $writer = new Csv($spreadsheet);
        $writer->save($fullPath);

        $reportFile->setFullPath($fullPath);
        $reportFile->setFileName($fileName);

        return $reportFile;
    }

}
