<?php

namespace App\Entity;

use App\Repository\EmployeeSalaryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity(repositoryClass=EmployeeSalaryRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class EmployeeSalary
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="basicSalary is required")
     */
    private $basicSalary;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $premium;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="salary is required")
     */
    private $salary;

    /**
     * @ORM\ManyToOne(targetEntity=DictCurrency::class, inversedBy="employeeSalary")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dictCurrency;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Employee::class, inversedBy="employee")
     * @ORM\JoinColumn(nullable=false)
     */
    private $employee;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBasicSalary(): ?float
    {
        return $this->basicSalary;
    }

    public function setBasicSalary(float $basicSalary): self
    {
        $this->basicSalary = $basicSalary;

        return $this;
    }

    public function getPremium(): ?float
    {
        return $this->premium;
    }

    public function setPremium(?float $premium): self
    {
        $this->premium = $premium;

        return $this;
    }

    public function getSalary(): ?float
    {
        return $this->salary;
    }

    public function setSalary(float $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getDictCurrency(): ?DictCurrency
    {
        return $this->dictCurrency;
    }

    public function setDictCurrency(?DictCurrency $dictCurrency): self
    {
        $this->dictCurrency = $dictCurrency;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
    
     /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }  
    
    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    public function getEmployee(): ?Employee
    {
        return $this->employee;
    }

    public function setEmployee(?Employee $employee): self
    {
        $this->employee = $employee;

        return $this;
    }
    

}
