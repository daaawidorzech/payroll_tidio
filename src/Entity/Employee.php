<?php

namespace App\Entity;

use App\Repository\EmployeeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EmployeeRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(indexes={
 *     @ORM\Index(name="idx_first_name", columns={"first_name"}),
 *     @ORM\Index(name="idx_last_name", columns={"last_name"})
 * })
 */
class Employee
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="firstName is required")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="lastName is required")
     */
    private $lastName;

    /**
     * @ORM\ManyToOne(targetEntity=DictDepartment::class, inversedBy="employee")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="dictDepartment is required")
     */
    private $dictDepartment;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="hiredAt is required")
     */
    private $hiredAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=EmployeeSalary::class, mappedBy="employee")
     */
    private $employee;

    

    public function __construct()
    {
        $this->employee = new ArrayCollection();
     
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDictDepartment(): ?DictDepartment
    {
        return $this->dictDepartment;
    }

    public function setDictDepartment(?DictDepartment $dictDepartment): self
    {
        $this->dictDepartment = $dictDepartment;

        return $this;
    }

    public function getHiredAt(): ?\DateTimeInterface
    {
        return $this->hiredAt;
    }

    public function setHiredAt(\DateTimeInterface $hiredAt): self
    {
        $this->hiredAt = $hiredAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
    
     /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }  
    
    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * @return Collection|EmployeeSalary[]
     */
    public function getEmployee(): Collection
    {
        return $this->employee;
    }

    public function addEmployee(EmployeeSalary $employee): self
    {
        if (!$this->employee->contains($employee)) {
            $this->employee[] = $employee;
            $employee->setEmployee($this);
        }

        return $this;
    }

    public function removeEmployee(EmployeeSalary $employee): self
    {
        if ($this->employee->removeElement($employee)) {
            // set the owning side to null (unless already changed)
            if ($employee->getEmployee() === $this) {
                $employee->setEmployee(null);
            }
        }

        return $this;
    }


}
