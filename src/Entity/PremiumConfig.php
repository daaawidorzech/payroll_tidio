<?php

namespace App\Entity;

use App\Repository\PremiumConfigRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Model\Types\PremiumTypeEnum;

/**
 * @ORM\Entity(repositoryClass=PremiumConfigRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class PremiumConfig
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=DictDepartment::class, inversedBy="premiumConfig")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="department is required")
     */
    private $dictDepartment;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="type is required")
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="value is required")
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity=DictCurrency::class, inversedBy="premiumConfig")
     * 
     */
    private $dictCurrency;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDictDepartment(): ?DictDepartment
    {
        return $this->dictDepartment;
    }

    public function setDictDepartment(?DictDepartment $dictDepartment): self
    {
        $this->dictDepartment = $dictDepartment;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        
        if (!in_array($type, PremiumTypeEnum::getTypes())) {
            throw new InvalidArgumentException("Invalid premium Type");
        }
        $this->type = $type;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDictCurrency(): ?DictCurrency
    {
        return $this->dictCurrency;
    }

    public function setDictCurrency(?DictCurrency $dictCurrency): self
    {
        $this->dictCurrency = $dictCurrency;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
    
    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }  
    
    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }
}
