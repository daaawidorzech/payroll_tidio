<?php

namespace App\Entity;

use App\Repository\DictDepartmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=DictDepartmentRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *     fields={"name"},
 *     message="The {{ value }} name is repeated."
 * )
 */
class DictDepartment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank(message="name is required")
     * 
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="description is required")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=Employee::class, mappedBy="dictDepartment")
     */
    private $employee;

    /**
     * @ORM\OneToMany(targetEntity=PremiumConfig::class, mappedBy="dictDepartment")
     */
    private $premiumConfig;

    public function __construct()
    {
        $this->employee = new ArrayCollection();
        $this->premiumConfig = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Employee[]
     */
    public function getEmployee(): Collection
    {
        return $this->employee;
    }

    public function addEmployee(Employee $employee): self
    {
        if (!$this->employee->contains($employee)) {
            $this->employee[] = $employee;
            $employee->setDictDepartment($this);
        }

        return $this;
    }

    public function removeEmployee(Employee $employee): self
    {
        if ($this->employee->removeElement($employee)) {
            // set the owning side to null (unless already changed)
            if ($employee->getDictDepartment() === $this) {
                $employee->setDictDepartment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PremiumConfig[]
     */
    public function getPremiumConfig(): Collection
    {
        return $this->premiumConfig;
    }

    public function addPremiumConfig(PremiumConfig $premiumConfig): self
    {
        if (!$this->premiumConfig->contains($premiumConfig)) {
            $this->premiumConfig[] = $premiumConfig;
            $premiumConfig->setDictDepartment($this);
        }

        return $this;
    }

    public function removePremiumConfig(PremiumConfig $premiumConfig): self
    {
        if ($this->premiumConfig->removeElement($premiumConfig)) {
            // set the owning side to null (unless already changed)
            if ($premiumConfig->getDictDepartment() === $this) {
                $premiumConfig->setDictDepartment(null);
            }
        }

        return $this;
    }
    
     /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }  
    
    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }
}
