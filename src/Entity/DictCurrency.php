<?php

namespace App\Entity;

use App\Repository\DictCurrencyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=DictCurrencyRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *     fields={"name"},
 *     message="The {{ value }} name is repeated."
 * )
 */
class DictCurrency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank(message="name is required")
     * 
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="description is required")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank(message="symbol is required")
     */
    private $symbol;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=PremiumConfig::class, mappedBy="dictCurrency")
     */
    private $premiumConfig;

    /**
     * @ORM\OneToMany(targetEntity=EmployeeSalary::class, mappedBy="dictCurrency")
     */
    private $employeeSalary;

    

    public function __construct()
    {
        $this->premiumConfig = new ArrayCollection();
        $this->employeeSalary = new ArrayCollection();
       
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|PremiumConfig[]
     */
    public function getPremiumConfig(): Collection
    {
        return $this->premiumConfig;
    }

    public function addPremiumConfig(PremiumConfig $premiumConfig): self
    {
        if (!$this->premiumConfig->contains($premiumConfig)) {
            $this->premiumConfig[] = $premiumConfig;
            $premiumConfig->setDictCurrency($this);
        }

        return $this;
    }

    public function removePremiumConfig(PremiumConfig $premiumConfig): self
    {
        if ($this->premiumConfig->removeElement($premiumConfig)) {
            // set the owning side to null (unless already changed)
            if ($premiumConfig->getDictCurrency() === $this) {
                $premiumConfig->setDictCurrency(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmployeeSalary[]
     */
    public function getEmployeeSalary(): Collection
    {
        return $this->employeeSalary;
    }

    public function addEmployeeSalary(EmployeeSalary $employeeSalary): self
    {
        if (!$this->employeeSalary->contains($employeeSalary)) {
            $this->employeeSalary[] = $employeeSalary;
            $employeeSalary->setDictCurrency($this);
        }

        return $this;
    }

    public function removeEmployeeSalary(EmployeeSalary $employeeSalary): self
    {
        if ($this->employeeSalary->removeElement($employeeSalary)) {
            // set the owning side to null (unless already changed)
            if ($employeeSalary->getDictCurrency() === $this) {
                $employeeSalary->setDictCurrency(null);
            }
        }

        return $this;
    }
    
    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
    }  
    
    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

 
}
