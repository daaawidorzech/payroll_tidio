<?php

namespace App\Controller;

use App\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Model\Normalizer\NotNullNormalizer;
use App\Model\Dto\AbstractDto;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\Stream;

class AbstractController {

    /**
     * Utworzenie response
     * @param array $dto
     * @return JsonResponse
     */
    public function getResponseSuccess($dto): JsonResponse {

        $encoders = [new JsonEncoder()];
        $normalizers = [new NotNullNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $json = $serializer->serialize($dto, 'json');
        $response = new JsonResponse($json, 200, [], true);

        return $response;
    }

    /**
     * Utworzenie response dla dodania rekordu
     * @param int $insertIdRow
     * @return JsonResponse
     */
    public function getResponseSuccessInsert(int $insertIdRow) {

        $response = [];
        $response['success'] = true;
        $response['data']['rowId'] = $insertIdRow;

        return $this->getResponseSuccess($response);
    }

    /**
     * Utworzenie domyślnego Response operacji zakończonej sukcesem
     * @return JsonResponse
     */
    public function getResponseSuccessDefault() {

        $response = [];
        $response['success'] = true;

        return $this->getResponseSuccess($response);
    }

    /**
     * Utworzenie response dla dodania rekordu
     * @param array $list
     * @param int|NULL $count
     * @return JsonResponse
     */
    public function getResponseSuccessList(array $list, int $count = NULL) {

        $response = [];
        $response['success'] = true;
        if (!is_null($count)) {
            $response['count'] = $count;
        }
        $response['data'] = $list;

        return $this->getResponseSuccess($response);
    }

    protected function getPostContent(Request $request) {

        $requestData = json_decode($request->getContent(), true);

        if (gettype($requestData) != "array") {
            throw new InvalidArgumentException("Brak body");
        }
        return $requestData;
    }

    protected function fromQuery(Request $request, AbstractDto $dto) {

        $data = [];
        parse_str($request->getQueryString(), $data);
        $dto->fromArray($data);

        return $dto;
    }

    protected function getReponseFile(string $filePath, string $fileName) {

        $file = new Stream($filePath);
        $response = new BinaryFileResponse($file);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName);
        $response->deleteFileAfterSend(true);

        return $response;
    }

}
