<?php

namespace App\Controller;

use App\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Dto\Employee\CreateEmployeeReq;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\Employee\EmployeeService;
use App\Validator\ObjectValidator;

class EmployeeController extends AbstractController {

    private EmployeeService $employeeService;
    private ObjectValidator $objectValidator;
    
    function __construct(
            EmployeeService $employeeService,
            ObjectValidator $objectValidator
    ) {
        $this->employeeService = $employeeService;
        $this->objectValidator = $objectValidator;
    }

    /**
     * Create employee
     * @Route("/employee", methods={"POST"})
     */
    public function createEmployee(Request $request): JsonResponse {
       
        $dto = new CreateEmployeeReq();
        $dto->fromArray($this->getPostContent($request));
        $this->objectValidator->validateEntity($dto);
        
        $this->employeeService->createEmployee($dto);
        return $this->getResponseSuccessDefault();
    }

}
