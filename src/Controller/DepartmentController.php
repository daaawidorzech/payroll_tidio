<?php

namespace App\Controller;

use App\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Dto\Department\CreateDepartmentReq;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\Department\DepartmentService;
use App\Validator\ObjectValidator;

class DepartmentController extends AbstractController {

    private DepartmentService $departmentService;
    private ObjectValidator $objectValidator;
    
    function __construct(
            DepartmentService $departmentService,
            ObjectValidator $objectValidator
    ) {
        $this->departmentService = $departmentService;
        $this->objectValidator = $objectValidator;
    }

    /**
     * Create Department
     * @Route("/department", methods={"POST"})
     */
    public function createDepartment(Request $request): JsonResponse {
       
        $dto = new CreateDepartmentReq();
        $dto->fromArray($this->getPostContent($request));
        $this->objectValidator->validateEntity($dto);
        
        $this->departmentService->createDepartment($dto);
        return $this->getResponseSuccessDefault();
    }

}
