<?php

namespace App\Controller;

use App\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Report\ReportService;
use App\Model\Dto\Report\Filter;
use App\Model\File\Report\AbstractReportFile;

class ReportController extends AbstractController {

    private ReportService $reportService;

    function __construct(
            ReportService $reportService
    ) {
        $this->reportService = $reportService;
    }

    /**
     * Create payroll export report
     * @Route("/report", methods={"GET"})
     */
    public function createEmployee(Request $request) {

        $filter = new Filter();
        $this->fromQuery($request, $filter);

        $response = $this->reportService->createReport($filter);

        return ($response instanceof AbstractReportFile ) ?
                $this->getReponseFile($response->getFullPath(), $response->getFileName()) :
                $this->getResponseSuccessList($response);
    }

}
