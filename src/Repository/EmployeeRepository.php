<?php

namespace App\Repository;

use App\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Model\Dto\Report\Filter;

/**
 * @method Employee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employee[]    findAll()
 * @method Employee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Employee::class);
    }

    public function findByFilters(Filter $filters) {
 
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.firstName as firstName', 'e.lastName as lastName', 'd.description as departmentName', 'es.basicSalary as basicSalary', 'es.premium as premium', 'pc.type as premiumType', 'es.salary as salary');
        $qb->join('e.employee', 'es');
        $qb->join('e.dictDepartment', 'd');
        $qb->join('d.premiumConfig', 'pc');
        $qb->join('es.dictCurrency', 'dc');

        if ($filters->getSearch()) {
            foreach ($filters->getSearch() as $name => $filter) {

                $qb->andWhere($filter["query"]);
                $qb->setParameter($name, $filter["value"]);
            }
        }
        if ($filters->getOrder()) {
            $qb->orderBy($filters->getOrder()["column"], $filters->getOrder()["direction"]);
        }

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return Employee[] Returns an array of Employee objects
    //  */
    /*
      public function findByExampleField($value)
      {
      return $this->createQueryBuilder('e')
      ->andWhere('e.exampleField = :val')
      ->setParameter('val', $value)
      ->orderBy('e.id', 'ASC')
      ->setMaxResults(10)
      ->getQuery()
      ->getResult()
      ;
      }
     */

    /*
      public function findOneBySomeField($value): ?Employee
      {
      return $this->createQueryBuilder('e')
      ->andWhere('e.exampleField = :val')
      ->setParameter('val', $value)
      ->getQuery()
      ->getOneOrNullResult()
      ;
      }
     */
}
