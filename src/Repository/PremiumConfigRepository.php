<?php

namespace App\Repository;

use App\Entity\PremiumConfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PremiumConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method PremiumConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method PremiumConfig[]    findAll()
 * @method PremiumConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PremiumConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PremiumConfig::class);
    }

    // /**
    //  * @return PremiumConfig[] Returns an array of PremiumConfig objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PremiumConfig
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
