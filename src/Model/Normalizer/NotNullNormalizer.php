<?php

namespace App\Model\Normalizer;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class NotNullNormalizer extends ObjectNormalizer
{
    public function normalize($object, $format = null, array $context = [])
    {
        $data = parent::normalize($object, $format, $context);

        return array_filter($data, function ($value) {
            return null !== $value;
        });
    }
}