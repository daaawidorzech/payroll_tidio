<?php

namespace App\Model\File\Report;

use App\Model\File\Report\AbstractReportFile;

class ReportFile extends AbstractReportFile {

    protected array $data = [];
  
    function getData(): array {
        return $this->data;
    }

    function setData(array $data): void {
        $this->data = $data;
    }

}
