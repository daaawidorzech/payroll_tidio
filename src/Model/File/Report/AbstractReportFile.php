<?php

namespace App\Model\File\Report;

abstract class AbstractReportFile {

    const COLUMNS = [array(
        'Imię', 'Nazwisko', 'Dział', 'Podstawa Wynagrodzenia (kwota)', 'Dodatek do podstawy (kwota)', 'Typ dodatku (typ % lub stały)', 'Wynagrodzenie wraz z dodatkiem (kwota)'
    )];

    protected string $fileName = "report";
    protected string $fullPath;

    function getFileName() {
        return $this->fileName;
    }

    function setFileName($fileName): void {
        $this->fileName = $fileName;
    }

    function getFullPath(): string {
        return $this->fullPath;
    }

    function setFullPath(string $fullPath): void {
        $this->fullPath = $fullPath;
    }

    abstract function getData();

    abstract function setData(array $data);
}
