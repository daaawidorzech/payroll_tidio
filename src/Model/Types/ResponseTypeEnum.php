<?php

namespace App\Model\Types;

use App\Exception\InvalidArgumentException;

class ResponseTypeEnum {

    const TYPE_RESPONSE_JSON = 'json';
    const TYPE_RESPONSE_CSV = 'csv';
    const TYPE_RESPONSE_XLSX = 'xlsx';

    private static $types = [
        self::TYPE_RESPONSE_JSON => self::TYPE_RESPONSE_JSON,
        self::TYPE_RESPONSE_CSV => self::TYPE_RESPONSE_CSV,
        self::TYPE_RESPONSE_XLSX => self::TYPE_RESPONSE_XLSX
    ];

    public static function getValue(string $typeString): string {
        if (!isset(self::$types[$typeString])) {
            throw new InvalidArgumentException(\sprintf('Invalid response type: %s', $typeString));
        }
        return self::$types[$type];
    }

    public static function getTypes() {
        return self::$types;
    }

    public static function fromValue(string $findType): string {

        foreach (self::$types as $key => $type) {
            if ($type == $findType) {
                return $key;
            }
        }
        throw new InvalidArgumentException(\sprintf('Invalid response type: %s', $findType));
    }

}
