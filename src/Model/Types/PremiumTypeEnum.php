<?php

namespace App\Model\Types;

use App\Exception\InvalidArgumentException;

class PremiumTypeEnum {

    const TYPE_CONST_AMOUNT = 'CONST_AMOUNT';
    const TYPE_PERCENT_AMOUNT = 'PERCENT_AMOUNT';

    private static $types = [
        self::TYPE_CONST_AMOUNT => self::TYPE_CONST_AMOUNT,
        self::TYPE_PERCENT_AMOUNT => self::TYPE_PERCENT_AMOUNT
    ];

    public static function getValue(string $typeString): string {
        if (!isset(self::$types[$typeString])) {
            throw new InvalidArgumentException(\sprintf('Invalid premium type: %s', $typeString));
        }
        return self::$types[$type];
    }

    public static function getTypes() {
        return self::$types;
    }

    public static function fromValue(string $findType): string {

        foreach (self::$types as $key => $type) {
            if ($type == $findType) {
                return $key;
            }
        }
        throw new InvalidArgumentException(\sprintf('Invalid premium type: %s', $findType));
    }

}
