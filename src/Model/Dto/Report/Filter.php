<?php

namespace App\Model\Dto\Report;

use App\Model\Dto\AbstractDto;
use App\Exception\InvalidArgumentException;
use App\Model\Types\ResponseTypeEnum;
use App\Exception\NotFoundException;

class Filter extends AbstractDto {

    protected array $search = [];
    protected array $order = [];
    protected string $responseType;

    const AVAILABLE_ORDERS = ['firstName', 'lastName', 'departmentName', 'basicSalary', 'premium', 'premiumType', 'salary'];
    const AVAILABLE_DIRECTIONS = ['asc', 'desc'];
    const FILTER_FIRSTNAME = "firstName";
    const FILTER_LASTNAME = "lastName";
    const FILTER_DEPARTMENTNAME = "departmentName";
    const FILTERS_QUERY = [
        self::FILTER_FIRSTNAME => [
            'query' => 'e.firstName LIKE :firstName',
            'value' => "%value%"
        ],
        self::FILTER_LASTNAME => [
            'query' => 'e.lastName LIKE :lastName',
            'value' => "%value%"
        ],
        self::FILTER_DEPARTMENTNAME => [
            'query' => 'd.name = :departmentName',
            'value' => "value"
        ],
    ];

    function getSearch(): array {
        return $this->search;
    }

    function setSearch(array $search): void {
        $this->search = $search;
    }

    function getOrder(): array {
        return $this->order;
    }

    function setOrder(array $order): void {
        $this->order = $order;
    }

    function getResponseType(): string {
        return $this->responseType;
    }

    function setResponseType(string $responseType): void {

        if (!in_array($responseType, ResponseTypeEnum::getTypes())) {
            throw new InvalidArgumentException("Invalid response Type");
        }
        $this->responseType = $responseType;
    }

    function fromArray(array $data): bool {

        if (!isset($data["responseType"])) {
            throw new NotFoundException("responseType is required.");
        }

        if (isset($data["filter"])) {
            $search = [];

            foreach ($data["filter"] as $name => $value) {
                $search[$name]['query'] = self::FILTERS_QUERY[$name]['query'];
                $search[$name]['value'] = $this->setQueryValue($value, $name);
            }
            $this->search = $search;
        }
        if (isset($data["order"]["column"]) && isset($data["order"]["direction"])) {

            $this->validateOrder($data["order"]);

            $order = [];
            $order["column"] = $data["order"]["column"];
            $order["direction"] = $data["order"]["direction"];
            $this->order = $order;
        }
        $this->setResponseType($data["responseType"]);

        return true;
    }

    function setQueryValue(string $value, string $name) {

        return str_replace('value', $value, self::FILTERS_QUERY[$name]['value']);
    }

    function validateOrder(array $order): bool {

        if(!isset($order["column"]) && !isset($order["direction"])){
            throw new InvalidArgumentException("order[column] and order[direction] are required");
        }
        if (!in_array($order["column"], self::AVAILABLE_ORDERS)) {
            throw new InvalidArgumentException("Invalid order column. Available:" . (json_encode(self::AVAILABLE_ORDERS)));
        }

        if (!in_array($order["direction"], self::AVAILABLE_DIRECTIONS)) {
            throw new InvalidArgumentException("Invalid order direction. Available:" . (json_encode(self::AVAILABLE_DIRECTIONS)));
        }

        return true;
    }

}
