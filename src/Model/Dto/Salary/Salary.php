<?php

namespace App\Model\Dto\Salary;

use App\Model\Dto\AbstractDto;
use Symfony\Component\Validator\Constraints as Assert;

class Salary extends AbstractDto {

    /**
     *
     * @Assert\NotBlank(message="basicSalary is required")
     */
    protected float $basicSalary;

    /**
     *
     * @Assert\NotBlank(message="currencyId is required")
     */
    protected int $currencyId;

    function getBasicSalary(): float {
        return $this->basicSalary;
    }

    function getCurrencyId(): int {
        return $this->currencyId;
    }

    function setBasicSalary(float $basicSalary): void {
        $this->basicSalary = $basicSalary;
    }

    function setCurrencyId(int $currencyId): void {
        $this->currencyId = $currencyId;
    }

}
