<?php

namespace App\Model\Dto\Department;

use App\Model\Dto\AbstractDto;
use Symfony\Component\Validator\Constraints as Assert;

class CreateDepartmentReq extends AbstractDto {

    protected string $name;
    protected string $description;
    protected string $currencyId;
    protected string $premiumType;
    protected float $premiumValue;

    function getName(): string {
        return $this->name;
    }

    function getDescription(): string {
        return $this->description;
    }

    function getCurrencyId(): string {
        return $this->currencyId;
    }

    function getPremiumType(): string {
        return $this->premiumType;
    }

    function getPremiumValue(): float {
        return $this->premiumValue;
    }

    function setName(string $name): void {
        $this->name = $name;
    }

    function setDescription(string $description): void {
        $this->description = $description;
    }

    function setCurrencyId(string $currencyId): void {
        $this->currencyId = $currencyId;
    }

    function setPremiumType(string $premiumType): void {
        $this->premiumType = $premiumType;
    }

    function setPremiumValue(float $premiumValue): void {
        $this->premiumValue = $premiumValue;
    }

}
