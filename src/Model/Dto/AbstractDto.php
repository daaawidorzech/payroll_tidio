<?php
namespace App\Model\Dto;

use App\Exception\InvalidArgumentException;

abstract class AbstractDto
{
    const FORMAT_DATETIME_STRING = 'Y-m-d H:i:s';
    const FORMAT_DATETIME_STRING_SHORT = 'Y-m-d H:i';
    const FORMAT_DATE_STRING_SHORT = 'Y-m-d';

    public function __toString(): string
    {
        $result = [];
        foreach (\get_object_vars($this) as $property => $value){
            $result[$property] = $value;
        }

        return \json_encode($result);
    }

    public function fromArray(array $data){
        
        foreach ($data as $property => $value)
        {   
            if (!array_key_exists($property, \get_class_vars(get_class($this)))){
                continue;
            }

            $this->$property = $value;
        }
    }

    public function toArray()
    {
        $properties = \get_class_vars(get_class($this));

        $result = [];
        foreach ($properties as $property => $value)
        {
            $result[$property] = $value;
        }

        return $result;
    }

    protected function parseDateTime(string $dateString = null): ?\DateTime
    {
        $date = \DateTime::createFromFormat(self::FORMAT_DATETIME_STRING, $dateString);

        if ($dateString && !$date instanceof \DateTime) {
            $date = \DateTime::createFromFormat(self::FORMAT_DATETIME_STRING_SHORT, $dateString);
        }

        if ($dateString && !$date instanceof \DateTime) {
            throw new InvalidArgumentException('Niepoprawny format daty, powinno być yyyy-mm-dd hh:ii:ss lub yyyy-mm-dd hh:ii (np 2019-02-12 09:30:00 lub 2019-02-12 09:30)');
        }

        if ($date instanceof \DateTime){
            return $date;
        }

        return null;
    }

    protected function parseDate(string $dateString = null): ?\DateTime
    {
        $date = \DateTime::createFromFormat('Y-m-d', $dateString);

        if ($dateString && !$date instanceof \DateTime) {
            throw new InvalidArgumentException('Niepoprawny format daty, powinno być yyyy-mm-dd (np 2019-02-12)');
        }

        if ($date instanceof \DateTime){
            return $date;
        }

        return null;
    }
}