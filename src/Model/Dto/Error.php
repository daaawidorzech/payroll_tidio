<?php

namespace App\Model\Dto;

use App\Model\Dto\AbstractDto;

class Error extends AbstractDto {

    protected int $errorId;
    protected string $errorText;
    protected ?string $errorTraceText;

    function getErrorId(): int {
        return $this->errorId;
    }

    function getErrorText(): string {
        return $this->errorText;
    }

    function getErrorTraceText(): ?string {
        return $this->errorTraceText;
    }

    function setErrorId(int $errorId): void {
        $this->errorId = $errorId;
    }

    function setErrorText(string $errorText): void {
        $this->errorText = $errorText;
    }

    function setErrorTraceText(?string $errorTraceText): void {
        $this->errorTraceText = $errorTraceText;
    }

}
