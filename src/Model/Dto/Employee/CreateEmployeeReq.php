<?php

namespace App\Model\Dto\Employee;

use App\Model\Dto\AbstractDto;
use App\Model\Dto\Salary\Salary;
use Symfony\Component\Validator\Constraints as Assert;

class CreateEmployeeReq extends AbstractDto {

    /**
     *
     * @Assert\NotBlank(message="firstName is required")
     */
    protected string $firstName;

    /**
     *
     * @Assert\NotBlank(message="lastName is required")
     */
    protected string $lastName;

    /**
     *
     * @Assert\NotBlank(message="departmentId is required")
     */
    protected int $departmentId;

    /**
     *
     * @Assert\NotBlank(message="basicSalary is required")
     */
    protected float $basicSalary;

    protected float $premium;

    /**
     *
     * @Assert\NotBlank(message="salary is required")
     */
    protected Salary $salary;

    /**
     *
     * @Assert\NotBlank(message="hiredAt is required")
     */
    protected string $hiredAt;

    function getFirstName(): string {
        return $this->firstName;
    }

    function getLastName(): string {
        return $this->lastName;
    }

    function getDepartmentId(): int {
        return $this->departmentId;
    }

    function getBasicSalary(): float {
        return $this->basicSalary;
    }

    function getPremium(): float {
        return $this->premium;
    }

    function getSalary(): Salary {
        return $this->salary;
    }

    function setFirstName(string $firstName): void {
        $this->firstName = $firstName;
    }

    function setLastName(string $lastName): void {
        $this->lastName = $lastName;
    }

    function setDepartmentId(int $departmentId): void {
        $this->departmentId = $departmentId;
    }

    function setBasicSalary(float $basicSalary): void {
        $this->basicSalary = $basicSalary;
    }

    function setPremium(float $premium): void {
        $this->premium = $premium;
    }

    function setSalary(Salary $salary): void {
        $this->salary = $salary;
    }

    function getHiredAt(): \DateTimeInterface {
        return $this->parseDate($this->hiredAt);
    }

    function setHiredAt(string $hiredAt): void {
        $this->hiredAt = $hiredAt;
    }

    public function fromArray(array $data) {
        parent::fromArray($data);

        $salary = new Salary();
        $salary->fromArray($data);
        $this->setSalary($salary);
    }

}
