<?php

namespace App\Model\SalaryPremium;

use App\Model\SalaryPremium\iSalaryPremium;

class ConstAmount implements iSalaryPremium {

    private float $premiumPerYear;
    private \DateTimeInterface $hiredAt;
    private int $constAmountYears;

    function __construct(
            float $premiumPerYear,
            \DateTimeInterface $hiredAt,
            int $constAmountYears
    ) {
        $this->premiumPerYear = $premiumPerYear;
        $this->hiredAt = $hiredAt;
        $this->constAmountYears = $constAmountYears;
    }

    public function getPremiumValue(): float {

        $now = new \DateTime();
        $diff = $this->hiredAt->diff($now);

        $diffInYears = $diff->y;

        return floatval(($diffInYears > $this->constAmountYears) ? bcmul($this->premiumPerYear, $this->constAmountYears, 2) : bcmul($this->premiumPerYear, $diffInYears, 2));
    }

}
