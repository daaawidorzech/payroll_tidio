<?php

namespace App\Model\SalaryPremium;

use App\Model\SalaryPremium\iSalaryPremium;

class PercentAmount implements iSalaryPremium {

    private float $basicSalary;
    private float $percentValue;

    function __construct(
            float $basicSalary,
            float $percentValue
    ) {
        $this->basicSalary = $basicSalary;
        $this->percentValue = $percentValue;
    }

    public function getPremiumValue(): float {

        return floatval(bcmul($this->basicSalary, $this->percentValue));
    }

}
