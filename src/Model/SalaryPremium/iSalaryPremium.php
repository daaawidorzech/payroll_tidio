<?php

namespace App\Model\SalaryPremium;

interface iSalaryPremium {

    public function getPremiumValue(): float;
}
