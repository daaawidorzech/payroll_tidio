<?php

namespace App\Listener;

use App\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConstraintViolationException;
use App\Exception\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Model\Dto\Error;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener {

    const ALLOWED_STATUS_CODES = [400, 401, 403, 404];

    private LoggerInterface $logger;
    private KernelInterface $kernel;

    public function __construct(
            LoggerInterface $logger,
            KernelInterface $kernel
    ) {
        $this->logger = $logger;
        $this->kernel = $kernel;
    }

    public function onKernelException(ExceptionEvent $event): Response {

        $exception = $event->getThrowable();

        $response = new JsonResponse();
        $response->setStatusCode(500);
        $dto = new Error();
        $dto->setErrorText($exception->getMessage());

        if ($exception instanceof InvalidArgumentException) {
            $response->setStatusCode(400);
            $dto->setErrorText($exception->getMessage());
        }

        if ($exception instanceof ConstraintViolationException) {
            $response->setStatusCode(400);
            $dto->setErrorText(\json_encode($exception->getViolations()));
        }

        if ($exception instanceof UnauthorizedException) {
            $response->setStatusCode(401);
            $dto->setErrorText($exception->getMessage());
        }

        if ($exception instanceof AccessDeniedHttpException) {
            $response->setStatusCode(403);
            $dto->setErrorText($exception->getMessage());
        }

        if ($exception instanceof NotFoundHttpException) {
            $response->setStatusCode(404);
            $dto->setErrorText($exception->getMessage());
        }
        $dto->setErrorId($response->getStatusCode());

        //maskujemy odpowiedź do API poza wyjątkami
        if (!$this->isPublicMessageAllowed($response)) {
            $dto->setErrorText('Wystąpił nieoczekiwany błąd, proszę powiadomić administratora');
        }

        if (!$this->isProdEnv()) {
            $dto->setErrorTraceText($exception->getTraceAsString());
        } else {
            $dto->removeErrorTraceText();
        }

        $response->setContent($dto);
        $event->setResponse($response);

        $this->logger->error("Exception", $this->getResponseErrorLog($exception));

        return $response;
    }

    private function isProdEnv(): bool {
        
        return $this->kernel->getEnvironment() == 'prod' ? true : false;
    }

    private function isPublicMessageAllowed(Response $response): bool {

        //dla testowych środowisk zwracaj wszystkie komunikaty
        if (!$this->isProdEnv()) {
            return true;
        }

        //dla nietestowych - tylko to na co klient ma wpływ
        return in_array($response->getStatusCode(), self::ALLOWED_STATUS_CODES);
    }

    private function getResponseErrorLog($exception): array {

        return [
            'error' => [
                'message' => $exception->getMessage(),
                'messageTrace' => $exception->getTraceAsString()
            ]
        ];
    }

}
