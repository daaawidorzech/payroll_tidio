<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210625224606 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE employee ADD dict_department_id INT NOT NULL');
        $this->addSql('ALTER TABLE employee ADD CONSTRAINT FK_5D9F75A1D5E94944 FOREIGN KEY (dict_department_id) REFERENCES dict_department (id)');
        $this->addSql('CREATE INDEX IDX_5D9F75A1D5E94944 ON employee (dict_department_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE employee DROP FOREIGN KEY FK_5D9F75A1D5E94944');
        $this->addSql('DROP INDEX IDX_5D9F75A1D5E94944 ON employee');
        $this->addSql('ALTER TABLE employee DROP dict_department_id');
    }
}
