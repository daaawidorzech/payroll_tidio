<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210625225749 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE premium_config ADD dict_currency_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE premium_config ADD CONSTRAINT FK_A630C8A1A23A91DE FOREIGN KEY (dict_currency_id) REFERENCES dict_currency (id)');
        $this->addSql('CREATE INDEX IDX_A630C8A1A23A91DE ON premium_config (dict_currency_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE premium_config DROP FOREIGN KEY FK_A630C8A1A23A91DE');
        $this->addSql('DROP INDEX IDX_A630C8A1A23A91DE ON premium_config');
        $this->addSql('ALTER TABLE premium_config DROP dict_currency_id');
    }
}
